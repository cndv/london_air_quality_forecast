# Air quality Forecast

## Methodology
    1. Data acqusition
        - Mannually 
    2. Exploration Visualisation 
        - Matplotlib/Pandas
    3. Outlier Detection and Removal
        - Visualisation, Removal using stastical tools such as mean and std
    4. Data Preparation for forecasting
        - Seperation (70/30)
        - Check correletation between lagged values
    5. Modeling
        - Univariate/Persistence Model (simple comparison with previous values)
        - Auto Linear Regression
        - RNN-LSTM Neural Network
    6. Evaluation
        - Using Root Square Error
    7. Deployment
        - Save/Export model and weights 

## Setup
### Create Virtual Enviroment
virtualenv venv

### Install Requirements
pip install -r requirements 

### Create jupyter kernel
python -m ipykernel install --user --name venv --display-name 'venv'

### Run notebook in specified port
jupyter lab --port=8889

### Improvements
- Data acqusition using api
- Ability to define an area in London and get data
- Provide forecast for more metrics currently only for (NO, PM10, PM2.5)
- Predict for custom threshold in the future currently set to 15'
- Data preperation using pandas
- Better visualisation for identifying outliers
- Better outlier removal and interpolation using neural networks
- Additional tuning for Neural Network to avoid overfitting
- 
